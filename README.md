#### Login to lxplus and clone the training repo.

cd /eos/user/${USER:0:1}/$USER/SWAN_projects  
git clone https://gitlab.cern.ch/db/sparkTraining.git  
touch .swanproject  

#### Login to SWAN  
https://swan.cern.ch/

##### Session 1
open sparkTraining->notebooks->Tutorial-DataFrame1-Final.ipynb  
open sparkTraining->notebooks->Tutorial-DataFrame2-Final.ipynb  

##### Session 2
open sparkTraining->notebooks->Tutorial-SparkSQL-Final.ipynb  
open sparkTraining->notebooks->Tutorial-SparkStreaming-Final.ipynb  

##### Session 3
open sparkTraining->notebooks->Tutorial-Analytix-Hostmetrics-Final.ipynb  
open sparkTraining->notebooks->Tutorial-NxCALS-Final.ipynb  
open sparkTraining->notebooks->Tutorial-RDD-Short-Final.ipynb  

##### Session 4
open sparkTraining->notebooks->ML_HandsOn_1.ipynb  
open sparkTraining->notebooks->ML_HandsOn_2.ipynb  
